const mongoosed = require('mongoose');
const URI = require('./config/cfg');

mongoosed.connect(URI.database.URI,{useNewUrlParser:true,useUnifiedTopology:true})
	.then(db => console.log('DB is Connected'))
	.catch(err => console.error(err));

module.exports = mongoosed;