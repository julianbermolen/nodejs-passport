const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const routes = require('./routes')


const app = express()
// Settings
app.set('port', process.env.PORT || 4000); 

//De este archivo, solo quiero la conexion, osea mongoose.
const { conexion } = require('./database');


require('./auth/auth')

app.use(bodyParser.json())
app.use(routes)




//Starting the server
app.listen(app.get('port'), () =>{
	console.log('Server on port', app.get('port'));

});