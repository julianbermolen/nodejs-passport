const passport = require('passport')
const localStrategy = require('passport-local').Strategy
const User = require('../model/User')
const FacebookStrategy = require('passport-facebook').Strategy
const JWTStrategy = require('passport-jwt').Strategy
const ExtractJWT = require('passport-jwt').ExtractJwt
const URI = require('../config/cfg');
const { json } = require('body-parser')


passport.use('signup', new localStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, async (email, password, done) => {
    User.findOne({'local.email': email}, function (err,user){
        if(err){return done(err)}
        if(user){
            return done(null,false,{message: 'El usuario ya existe'})
        } else {
            var user = new User()
            user.local.email = email
            user.local.password = generateHash(password)
            user.save(function(err){
                if(err){throw err}
                return done(null, user)
            })
        }
    })
}))

passport.use('login', new localStrategy({
    usernameField: 'email',
    passwordField: 'password',
}, async (email, password, done) => {
    try {
        const user = await User.findOne({ 'local.email': email })
        if (!user) {
            return done(null, false, { message: 'User not found' })
        }

        const validate = await user.isValidPassword(password)

        if (!validate) {
            return done(null, false, { message: 'Wrong password' })
        }

        return done(null, user, { message: 'Login successfull' })
    } catch (e) {
        return done(e)
    }
}))

passport.use(new JWTStrategy({
    secretOrKey: 'top_secret',
    jwtFromRequest: ExtractJWT.fromUrlQueryParameter('secret_token')
}, async (token, done) => {
    try {
        return done(null, token.user)
    } catch (e) {
        done(error)
    }
}))

passport.use(new FacebookStrategy({
    clientID: URI.facebook.FBClientId,
    clientSecret: URI.facebook.FBClientSecret,
    callbackURL: '/auth/facebook/callback'
},
    function(accessToken, refreshToken, profile, cb){
        User.findOne({'facebook.id': profile.id}, function (err, user){
            if(err){throw err}
            if(user){return user}

            var user = new User();
            user.facebook.id = profile.id
            user.nombre = profile.name
            user.email = profile.email
            user.imagen = profile.photos
            
            user.save(function(err){
                if(err){throw err}
                return cb(null, user)
            })
        })
    }))

