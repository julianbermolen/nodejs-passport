const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const Schema = mongoose.Schema

const UserSchema = new Schema({
    local: {
        email: {type: String, unique: true },
        password: {type: String,}
    },
    facebook: {
        id: {type: String, unique: true},
        nombre: {type: String},
        imagen: {type: String},
        email: {type: String}
    }
})

UserSchema.methods.generateHash = function (password){
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8),null);
}
UserSchema.methods.isValidPassword = async function(password) {
    const user = this;
    const compare = await bcrypt.compare(password, this.local.password)
    return compare
}

module.exports = mongoose.model('User', UserSchema)
